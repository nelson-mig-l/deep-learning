package ai.nelson;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        // D:\repository\java\deep-learning\data\viceroy-butterfly_1600.jpg
        BufferedImage in = ImageIO.read(new File(args[0]));

        final EdgeDetector edgeDetector = new EdgeDetector();
        for (final Filters f : Filters.values()) {
            final BufferedImage out = edgeDetector.detectEdges(in, f);
            File outputFile = new File("D:\\repository\\java\\deep-learning\\data\\tmp"+f.name()+".png");
            ImageIO.write(out, "png", outputFile);
        }
        //return outputFile;
    }

}
