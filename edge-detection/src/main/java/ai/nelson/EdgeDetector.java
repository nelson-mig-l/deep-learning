package ai.nelson;

import java.awt.image.BufferedImage;

public class EdgeDetector {

    public BufferedImage detectEdges(final BufferedImage image, final Filters filter) {
        final double[][][] rgb = ImageUtils.toArray(image);
        final double[][] convolved = applyConvolution(image.getWidth(), image.getHeight(), rgb, filter.getKernel());
        return ImageUtils.toBufferedImage(convolved);
    }

    private double[][] applyConvolution(int width, int height, double[][][] rgb, double[][] filter) {
        Convolution convolution = new Convolution();
        double[][] redConv = convolution.convolutionType2(rgb[0], height, width,
                filter, 3, 3, 1);
        double[][] greenConv = convolution.convolutionType2(rgb[1], height,
                width, filter, 3, 3, 1);
        double[][] blueConv = convolution.convolutionType2(rgb[2], height,
                width, filter, 3, 3, 1);
        double[][] finalConv = new double[redConv.length][redConv[0].length];
        for (int i = 0; i < redConv.length; i++) {
            for (int j = 0; j < redConv[i].length; j++) {
                finalConv[i][j] = redConv[i][j] + greenConv[i][j] + blueConv[i][j];
            }
        }
        return finalConv;
    }
}
