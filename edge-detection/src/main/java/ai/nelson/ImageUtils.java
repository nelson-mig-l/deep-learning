package ai.nelson;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageUtils {

    public static double[][][] toArray(BufferedImage bufferedImage) {
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        double[][][] arr = new double[3][height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                Color color = new Color(bufferedImage.getRGB(j, i));
                arr[0][i][j] = color.getRed();
                arr[1][i][j] = color.getGreen();
                arr[2][i][j] = color.getBlue();
            }
        }
        return arr;
    }

    public static BufferedImage toBufferedImage(double[][] arr) {
        BufferedImage writeBackImage = new BufferedImage(arr[0].length,
                arr.length, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                Color color = new Color(
                        fixOutOfRangeRGBValues(arr[i][j]),
                        fixOutOfRangeRGBValues(arr[i][j]),
                        fixOutOfRangeRGBValues(arr[i][j]));
                writeBackImage.setRGB(j, i, color.getRGB());
            }
        }

        return writeBackImage;
    }

    private static int fixOutOfRangeRGBValues(double value) {
        if (value < 0.0) {
            value = -value;
        }
        if (value > 255) {
            return 255;
        } else {
            return (int) value;
        }
    }

}
