package ai.nelson;

public enum Filters {

    HORIZONTAL {
        @Override
        public double[][] getKernel() {
            final double[][] kernel = {{1, 1, 1}, {0, 0, 0}, {-1, -1, -1}};
            return kernel;
        }
    },
    VERTICAL {
        @Override
        public double[][] getKernel() {
            final double[][] kernel = {{1, 0, -1}, {1, 0, -1}, {1, 0, -1}};
            return kernel;
        }
    },
    SOBEL_HORIZONTAL {
        @Override
        public double[][] getKernel() {
            final double[][] kernel = {{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}};
            return kernel;
        }
    },
    SOBEL_VERTICAL {
        @Override
        public double[][] getKernel() {
            final double[][] kernel = {{1, 0, -1}, {2, 0, -2}, {1, 0, -1}};
            return kernel;
        }
    },
    SCHARR_HORIZONTAL {
        @Override
        public double[][] getKernel() {
            final double[][] kernel = {{3, 10, 3}, {0, 0, 0}, {-3, -10, -3}};
            return kernel;
        }
    },
    SCHARR_VERTICAL {
        @Override
        public double[][] getKernel() {
            final double[][] kernel = {{3, 0, -3}, {10, 0, -10}, {3, 0, -3}};
            return kernel;
        }
    };

    public abstract double[][] getKernel();

    public static Filters get() {

        return Filters.HORIZONTAL;
    }
}
