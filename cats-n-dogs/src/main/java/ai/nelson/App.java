package ai.nelson;

import static ai.nelson.DataReader.NUM_POSSIBLE_LABELS;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.zip.Adler32;

import org.apache.commons.io.FileUtils;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.transferlearning.FineTuneConfiguration;
import org.deeplearning4j.nn.transferlearning.TransferLearning;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.PretrainedType;
import org.deeplearning4j.zoo.model.VGG16;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class App {

    private static final int SAVING_INTERVAL = 100;

    /**
     * Number of total traverses through data.
     * with 5 epochs we will have 5/@MINI_BATCH_SIZE iterations or weights updates
     */
    private static final int EPOCH = 5;


    /**
     * The alpha learning rate defining the size of step towards the minimum
     */
    private static final double LEARNING_RATE = 5e-5;

    /**
     * The layer where we need to stop back propagating
     */
    private static final String FREEZE_UNTIL_LAYER = "fc2";

    private static final String SAVING_PATH = "C:\\repository\\nelson\\deep-learning\\data\\catvsdog\\models\\iter_";

    // https://github.com/klevis/Java-Machine-Learning-for-Computer-Vision/blob/master/CatVsDogRecognition/src/main/java/ramo/klevis/ml/vg16/TransferLearningVGG16.java
    public static void main(String[] args) throws IOException {
        /*
        final VGG16 vgg = VGG16.builder().build();
        System.out.println("vgg...");
        final Model model = vgg.initPretrained(PretrainedType.IMAGENET);
        System.out.println("vgg ready");
        System.out.println(model.conf());

        FineTuneConfiguration fineTuneConf = new FineTuneConfiguration.Builder()
            //.learningRate(LEARNING_RATE)
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
            .updater(new Nesterovs(LEARNING_RATE, Nesterovs.DEFAULT_NESTEROV_MOMENTUM))
            //.updater(Updater.NESTEROVS)
            .seed(1234)
            .build();

        final ComputationGraph preTrainedNet = (ComputationGraph)model;
        System.out.println(preTrainedNet.summary());
        ComputationGraph vgg16Transfer = new TransferLearning.GraphBuilder(preTrainedNet)
            .fineTuneConfiguration(fineTuneConf)
            .setFeatureExtractor(FREEZE_UNTIL_LAYER)
            .removeVertexKeepConnections("predictions")
            .addLayer("predictions",
                new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                    .nIn(4096)
                    .nOut(NUM_POSSIBLE_LABELS)
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.SOFTMAX)
                    .build(),
                FREEZE_UNTIL_LAYER)
            .build();
        */
        final ComputationGraph vgg16Transfer = ModelSerializer
            .restoreComputationGraph("C:\\repository\\nelson\\deep-learning\\data\\catvsdog\\models\\iter_600_epoch_0.zip");

        vgg16Transfer.setListeners(new ScoreIterationListener(5));

        System.out.println(vgg16Transfer.summary());

        DataReader dr = new DataReader(
            "C:\\repository\\nelson\\deep-learning\\data\\catvsdog\\train_both",
            "C:\\repository\\nelson\\deep-learning\\data\\catvsdog\\test_both");

        int iEpoch = 0;
        int iIteration = 601;
        while (iEpoch < EPOCH) {
            while (true) {
                final DataSetIterator trainIterator = dr.getTrainIterator();
                if (!trainIterator.hasNext())
                    break;
                DataSet trainMiniBatchData = dr.getTrainIterator().next();
                vgg16Transfer.fit(trainMiniBatchData);
                saveProgressEveryConfiguredInterval(dr, vgg16Transfer, iEpoch, iIteration);
                iIteration++;
            }
            dr.getTrainIterator().reset();
            iEpoch++;

            evalOn(vgg16Transfer, dr.getTestIterator(), iEpoch);
        }

    }

    private static void saveProgressEveryConfiguredInterval(DataReader dr, ComputationGraph vgg16Transfer, int iEpoch, int
        iIteration) throws IOException {
        if (iIteration % SAVING_INTERVAL == 0 && iIteration != 0) {

            ModelSerializer.writeModel(vgg16Transfer, new File(SAVING_PATH + iIteration + "_epoch_" + iEpoch + ".zip"),
                false);
            evalOn(vgg16Transfer, dr.getDevIterator(), iIteration);
        }
    }

    private static void evalOn(ComputationGraph vgg16Transfer, DataSetIterator testIterator, int iEpoch) throws IOException {
        System.out.println("Evaluate model at iteration " + iEpoch + " ....");
        Evaluation eval = vgg16Transfer.evaluate(testIterator);
        System.out.println(eval.stats());
        testIterator.reset();

    }
}
