package ai.nelson;

import static org.datavec.image.loader.BaseImageLoader.ALLOWED_FORMATS;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.BaseImageLoader;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.VGG16ImagePreProcessor;

public class DataReader {

    int HEIGHT = 224;
    int WIDTH = 224;
    int CHANNELS = 3;

    Random RAND_NUM_GEN = new Random(12345);
    ParentPathLabelGenerator LABEL_GENERATOR_MAKER = new ParentPathLabelGenerator();
    BalancedPathFilter PATH_FILTER = new BalancedPathFilter(RAND_NUM_GEN, ALLOWED_FORMATS, LABEL_GENERATOR_MAKER);

    /**
     * Number prediction classes.
     * To predict if an image is a cat or dog we need to classes
     */
    public static int NUM_POSSIBLE_LABELS = 2;

    /**
     * Mini batch gradient descent size or number of matrices processed in parallel.
     * For CORE-I7 16 is good for GPU please change to 128 and up
     */
    int BATCH_SIZE = 16;

    /**
     * The parentage of available data used for training.
     * 85% for training and 15% for development(dev) data set
     * Note: There is also a test data set where we test after each epoch
     * how the network generalizes to unbiased data
     */
    int TRAIN_SIZE = 85;

    private final DataSetIterator trainIterator;
    private final DataSetIterator devIterator;
    private final DataSetIterator testIterator;

    public DataReader(String trainDirectoryPath, String testDirectoryPath) {
        final FileSplit trainData =
            new FileSplit(new File(trainDirectoryPath), BaseImageLoader.ALLOWED_FORMATS, RAND_NUM_GEN);
        InputSplit[] trainAndDevData = trainData.sample(PATH_FILTER, TRAIN_SIZE, 100 - TRAIN_SIZE);
        FileSplit testData = new FileSplit(new File(testDirectoryPath), BaseImageLoader.ALLOWED_FORMATS, RAND_NUM_GEN);
        try {
            trainIterator = dsi(trainAndDevData[0]);
            devIterator = dsi(trainAndDevData[1]);

            testIterator = dsi(testData.sample(PATH_FILTER, 1, 0)[0]);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public DataSetIterator getTrainIterator() {
        return trainIterator;
    }

    public DataSetIterator getDevIterator() {
        return devIterator;
    }

    public DataSetIterator getTestIterator() {
        return testIterator;
    }

    private DataSetIterator dsi(InputSplit sample) throws IOException {
        final ImageRecordReader imageRecordReader = new ImageRecordReader(HEIGHT, WIDTH, CHANNELS, LABEL_GENERATOR_MAKER);
        imageRecordReader.initialize(sample);
        final RecordReaderDataSetIterator iterator =
            new RecordReaderDataSetIterator(imageRecordReader, BATCH_SIZE, 1, NUM_POSSIBLE_LABELS);
        iterator.setPreProcessor(new VGG16ImagePreProcessor());
        return iterator;
    }
}
