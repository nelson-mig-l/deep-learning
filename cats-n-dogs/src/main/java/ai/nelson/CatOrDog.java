package ai.nelson;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.VGG16ImagePreProcessor;

public class CatOrDog {

    public static void main(String[] args) throws IOException {
        new CatOrDog().run(args[0]);
    }

    public void run(final String file) throws IOException {
        final ComputationGraph model = ModelSerializer.restoreComputationGraph(
            new File("C:\\repository\\nelson\\deep-learning\\data\\catvsdog\\models\\iter_600_epoch_0.zip"));
        model.init();
        System.out.println(model.summary());
        final INDArray image = imageFileToMatrix(new File(file));
        final INDArray output = model.outputSingle(image);
        final double cat = output.getDouble(0);
        final double dog = output.getDouble(1);
        if (cat > dog) System.out.println("cat"); else System.out.println("dog");
    }

    private INDArray imageFileToMatrix(File file) throws IOException {
        NativeImageLoader loader = new NativeImageLoader(224, 224, 3);
        INDArray image = loader.asMatrix(new FileInputStream(file));
        DataNormalization dataNormalization = new VGG16ImagePreProcessor();
        dataNormalization.transform(image);
        return image;
    }


}
