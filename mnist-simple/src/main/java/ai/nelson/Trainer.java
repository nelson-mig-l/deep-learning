package ai.nelson;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

public class Trainer {

    private final MultiLayerNetwork network;

    public Trainer(final MultiLayerNetwork network) {
        this.network = network;
        this.network.setListeners(new ScoreIterationListener(100));
        this.network.init();
    }

    public Evaluation train(DataSetIterator trainData, DataSetIterator testData) {
        network.fit(trainData);
        return network.evaluate(testData);
    }
}
