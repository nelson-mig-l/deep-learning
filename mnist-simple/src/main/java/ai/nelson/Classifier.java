package ai.nelson;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.factory.Nd4j;

public class Classifier {

    private final MultiLayerNetwork model;

    public Classifier(final MultiLayerNetwork model) {
        this.model = model;
    }

    public int predict(Image image) {
        int[] predict = model.predict(Nd4j.create(image.getNormalizedPixelsArr()));
        return predict[0];
    }
}
