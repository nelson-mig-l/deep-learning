package ai.nelson;

import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.earlystopping.scorecalc.ScoreCalculator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import java.util.Date;

public class AccuracyCalculator implements ScoreCalculator<MultiLayerNetwork> {

    private int i = 0;

    private final MnistDataSetIterator dataSetIterator;



    public AccuracyCalculator(MnistDataSetIterator dataSetIterator) {
        this.dataSetIterator = dataSetIterator;
    }

    @Override
    public double calculateScore(MultiLayerNetwork network) {
        Evaluation evaluate = network.evaluate(dataSetIterator);
        double accuracy = evaluate.accuracy();
        System.out.println(new Date().toString());
        System.err.println("Accuracy " + i++ + " : " + accuracy);
        return 1 - evaluate.accuracy();
    }

    @Override
    public boolean minimizeScore() {
        return true;
    }
}
