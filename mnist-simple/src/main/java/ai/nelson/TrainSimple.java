package ai.nelson;

import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;
import java.io.IOException;

public class TrainSimple {

    /**
     * https://en.wikipedia.org/wiki/Random_seed
     */
    //private static final int SEED = 123;

    /**
     * Number of total traverses through data.
     * with 5 epochs we will have 5/@MINI_BATCH_SIZE iterations or weights updates
     */
    private static final int EPOCHS = 5;

    /**
     * Mini batch gradient descent size or number of matrices processed in parallel.
     * For CORE-I7 16 is good for GPU please change to 128 and up
     */
    private static final int MINI_BATCH_SIZE = 16;// Number of training epochs

    private static final String OUTPUT_DIRECTORY = "C:\\repository\\nelson\\deep-learning\\mnist-simple\\src\\main\\resources\\simple.zip";



    public static void main(String[] args) throws IOException {
        final MultiLayerNetwork network = NetworkFactory.simple();
        final Trainer trainer = new Trainer(network);

        final int seed = (int) (Math.random() * Integer.MAX_VALUE);
        DataSetIterator trainData = new MnistDataSetIterator(MINI_BATCH_SIZE, true, seed);
        DataSetIterator testData = new MnistDataSetIterator(MINI_BATCH_SIZE, false, seed);
        for (int i = 0; i < EPOCHS; i++) {
            final Evaluation eval = trainer.train(trainData, testData);
            System.err.println(eval.getConfusionMatrix().toCSV());
            if (eval.accuracy() > 0.97) {
                File locationToSave = new File(OUTPUT_DIRECTORY);
                ModelSerializer.writeModel(network, locationToSave, true);
                break;
            }
            testData.reset();
        }
        //File locationToSave = new File(OUTPUT_DIRECTORY);
        //ModelSerializer.writeModel(network.getModel(), locationToSave, true);
    }
}
