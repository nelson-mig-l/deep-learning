package ai.nelson;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class NetworkFactory {

    /**
     * The alpha learning rate defining the size of step towards the minimum
     */
    private static final double LEARNING_RATE = 0.01;

    /**
     * Number prediction classes.
     * We have 0-9 digits so 10 classes in total.
     */
    private static final int OUTPUT = 10;

    private static final int IMAGE_WIDTH = 28;
    private static final int IMAGE_HEIGHT = 28;
    private static final int IMAGE_CHANNELS = 1;


    public static MultiLayerNetwork simple() {
        return new MultiLayerNetwork(createSimpleConfiguration());
    }

    public static MultiLayerNetwork convolutional() {
        return new MultiLayerNetwork(createConvolutionalConfiguration());
    }

    public static MultiLayerNetwork fromFile(final String file) throws IOException {
        return ModelSerializer.restoreMultiLayerNetwork(file);
    }

    private static MultiLayerConfiguration createSimpleConfiguration() {
        return new NeuralNetConfiguration.Builder()
                .seed(getRandomSeed())
                .updater(new Nesterovs(LEARNING_RATE, Nesterovs.DEFAULT_NESTEROV_MOMENTUM))
                //.learningRate(LEARNING_RATE)
                .weightInit(WeightInit.XAVIER)
                //NESTEROVS is referring to gradient descent with momentum
                .updater(Updater.NESTEROVS)
                .list()
                /**
                 * First hidden layer uses 128 neurons each with RELU activation.
                 * Is called dense layer because every neuron is linked with
                 * every other neuron on next layer and previous layer
                 */
                .layer(0, new DenseLayer.Builder().activation(Activation.RELU)
                        .nIn(IMAGE_WIDTH * IMAGE_HEIGHT)
                        .nOut(128).build())
                /**
                 * Second hidden layer uses 64 neurons each with RELU activation.
                 */
                .layer(1, new DenseLayer.Builder().activation(Activation.RELU)
                        .nOut(64).build())
                /**
                 * The output layer using SOFTMAX to predict 10 classes(OUTPUT)
                 * NEGATIVELOGLIKELIHOOD is just a cost function measuring how
                 * good our prediction or hypothesis is doing against real digits
                 */
                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(OUTPUT)
                        .activation(Activation.SOFTMAX)
                        .build())
                //.backprop(true)
                .backpropType(BackpropType.Standard)
                //.pretrain(false)
                .setInputType(InputType.convolutionalFlat(IMAGE_WIDTH, IMAGE_HEIGHT, 1))
                .build();
    }

    private static MultiLayerConfiguration createConvolutionalConfiguration() {
        return new NeuralNetConfiguration.Builder()
                .seed(getRandomSeed())
                //.iterations(1) // Number of training iteration
                //.regularization(false)
                //.learningRate(0.01)
                .updater(new Nesterovs(0.01, Nesterovs.DEFAULT_NESTEROV_MOMENTUM))
                .weightInit(WeightInit.XAVIER)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                //.updater(Updater.NESTEROVS)
                .list()
                .layer(0, new ConvolutionLayer.Builder(5, 5)
                        .nIn(IMAGE_CHANNELS)
                        .stride(1, 1)
                        .nOut(20)
                        .activation(Activation.IDENTITY)
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build())
                .layer(2, new ConvolutionLayer.Builder(5, 5)
                        .nIn(20)
                        .stride(1, 1)
                        .nOut(50)
                        .activation(Activation.IDENTITY)
                        .build())
                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build())
                .layer(4, new DenseLayer.Builder().activation(Activation.RELU)
                        .nIn(800)
                        .nOut(128).build())
                .layer(5, new DenseLayer.Builder().activation(Activation.RELU)
                        .nIn(128)
                        .nOut(64).build())
                .layer(6, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(OUTPUT)
                        .activation(Activation.SOFTMAX)
                        .build())
                .setInputType(InputType.convolutionalFlat(IMAGE_WIDTH, IMAGE_HEIGHT, 1))
                //.backprop(true)
                .backpropType(BackpropType.Standard)
                //.pretrain(false)
                .build();
    }

    private static long getRandomSeed() {
        return (long) (Math.random() * Long.MAX_VALUE);
    }

}
