package ai.nelson;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class Network {

//    /**
//     * https://en.wikipedia.org/wiki/Random_seed
//     */
//    private static final int SEED = 123;
//
//    private static final int CHANNELS = 1;
//
//    /**
//     * Number prediction classes.
//     * We have 0-9 digits so 10 classes in total.
//     */
//    private static final int OUTPUT = 10;
//
//
//    /**
//     * Number of total traverses through data. In this case it is used as the maximum epochs we allow
//     * with 5 epochs we will have 5/@MINI_BATCH_SIZE iterations or weights updates
//     */
//    private static final int MAX_EPOCHS = 20;

//    /**
//     * The alpha learning rate defining the size of step towards the minimum
//     */
//    private static final double LEARNING_RATE = 0.01;
//
//
//
//    private static final int IMAGE_WIDTH = 28;
//    private static final int IMAGE_HEIGHT = 28;

//    private final MultiLayerNetwork model;

//    public Network() {
//        this(new MultiLayerNetwork(createConfiguration()));
//    }

//    private Network(MultiLayerNetwork model) {
//        this.model = model;
//        model.setListeners(new ScoreIterationListener(100));
//        model.init();
//    }

//    public Evaluation train(DataSetIterator trainData, DataSetIterator testData) {
//        model.fit(trainData);
//        return model.evaluate(testData);
//    }

//    private static MultiLayerConfiguration createConfiguration() {
//        return new NeuralNetConfiguration.Builder()
//                .seed(SEED)
//                .learningRate(LEARNING_RATE)
//                .weightInit(WeightInit.XAVIER)
//                //NESTEROVS is referring to gradient descent with momentum
//                .updater(Updater.NESTEROVS)
//                .list()
//                /**
//                 * First hidden layer uses 128 neurons each with RELU activation.
//                 * Is called dense layer because every neuron is linked with
//                 * every other neuron on next layer and previous layer
//                 */
//                .layer(0, new DenseLayer.Builder().activation(Activation.RELU)
//                        .nIn(IMAGE_WIDTH * IMAGE_HEIGHT)
//                        .nOut(128).build())
//                /**
//                 * Second hidden layer uses 64 neurons each with RELU activation.
//                 */
//                .layer(1, new DenseLayer.Builder().activation(Activation.RELU)
//                        .nOut(64).build())
//                /**
//                 * The output layer using SOFTMAX to predict 10 classes(OUTPUT)
//                 * NEGATIVELOGLIKELIHOOD is just a cost function measuring how
//                 * good our prediction or hypothesis is doing against real digits
//                 */
//                .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
//                        .nOut(OUTPUT)
//                        .activation(Activation.SOFTMAX)
//                        .build())
//                .backprop(true)
//                .pretrain(false)
//                .build();
//    }
//
//
//    public Model getModel() {
//        return model;
//    }
}
