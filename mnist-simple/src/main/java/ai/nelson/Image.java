package ai.nelson;

import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class Image {

    private final double[] meanNormalizedPixels;
    private final double[] pixels;
    private final int width;
    private final int height;


    public static Image fromJavaFx(javafx.scene.image.Image input) {
        final int size = (int)(input.getWidth() * input.getHeight());
        double[] pixels = new double[size];
        int i = 0;
        final PixelReader pixelReader = input.getPixelReader();
        for (int y = 0; y < input.getHeight(); y++) {
           for (int x = 0; x < input.getWidth(); x ++) {
               final Color color = pixelReader.getColor(x, y);
               double red = (color.getRed());
               double green = (color.getGreen());
               double blue = (color.getBlue());
               //RGB-> black and white or gray scale
               double v = 255 - (red + green + blue) / 3d;
               pixels[i] = v;
               i++;
           }
        }
        return new Image(pixels, (int)(input.getWidth()), (int)(input.getHeight()));
    }

    public Image(double[] pixels, int width, int height) {
        this.meanNormalizedPixels = meanNormalizeFeatures(pixels);
        this.pixels = pixels;
        this.width = width;
        this.height = height;
    }

    public double[] getPixels() {
        return pixels;
    }

    public double[] getNormalizedPixels() {
        return meanNormalizedPixels;
    }

    public double[][] getNormalizedPixelsArr() {
        final double[] vector = getNormalizedPixels();
        double[][] output = new double[1][vector.length];
        output[0] = vector;
        return output;
    }


    private double[] meanNormalizeFeatures(double[] pixels) {
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double sum = 0;
        for (double pixel : pixels) {
            sum = sum + pixel;
            if (pixel > max) {
                max = pixel;
            }
            if (pixel < min) {
                min = pixel;
            }
        }
        double mean = sum / pixels.length;

        double[] pixelsNorm = new double[pixels.length];
        for (int i = 0; i < pixels.length; i++) {
            pixelsNorm[i] = (pixels[i] - mean) / (max - min);
        }
        return pixelsNorm;
    }

}
