package ai.nelson;

import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;
import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxTimeIterationTerminationCondition;
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TrainConvolutional {

    private static final int MAX_EPOCHS = 40;

    private static final String OUTPUT_DIRECTORY = "C:\\repository\\nelson\\deep-learning\\mnist-simple\\src\\main\\resources";


    public static void main(String[] args) throws IOException {
        System.out.println("Loading C:\\repository\\nelson\\deep-learning\\mnist-simple\\src\\main\\resources\\bestModel.bin");
        //60000, 1000
        //Integer trainDataSize, Integer testDataSize
        int trainDataSize = 60000;
        int testDataSize = 1000;
        int batchSize = 64; // Test batch size

        final MultiLayerNetwork network = NetworkFactory.convolutional();
        //final MultiLayerNetwork network = NetworkFactory.fromFile("C:\\repository\\nelson\\deep-learning\\mnist-simple\\src\\main\\resources\\bestModel.bin");

        MnistDataSetIterator mnistTrain = new MnistDataSetIterator(batchSize, trainDataSize, false, true, true, 12345);


        final AccuracyCalculator accuracyCalculator = new AccuracyCalculator(
                new MnistDataSetIterator(testDataSize, testDataSize, false, false, true, 12345));
        EarlyStoppingConfiguration esConf = new EarlyStoppingConfiguration.Builder()
                .epochTerminationConditions(new MaxEpochsTerminationCondition(MAX_EPOCHS))
                .iterationTerminationConditions(new MaxTimeIterationTerminationCondition(75, TimeUnit.MINUTES))
                .scoreCalculator(accuracyCalculator)
                .evaluateEveryNEpochs(1)
                .modelSaver(new LocalFileModelSaver(OUTPUT_DIRECTORY))
                .build();

        EarlyStoppingTrainer trainer = new EarlyStoppingTrainer(esConf, network, mnistTrain);


        EarlyStoppingResult result = trainer.fit();

        System.out.println("Termination reason: " + result.getTerminationReason());
        System.out.println("Termination details: " + result.getTerminationDetails());
        System.out.println("Total epochs: " + result.getTotalEpochs());
        System.out.println("Best epoch number: " + result.getBestModelEpoch());
        System.out.println("Score at best epoch: " + result.getBestModelScore());
    }
}
