package ai.nelson;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.Stage;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import java.io.IOException;


public class Example extends Application {

    private static final String MODEL_PATH = "C:\\repository\\nelson\\deep-learning\\mnist-simple\\src\\main\\resources\\";

    private final Classifier simpleClassifier;
    private final Classifier convolutionalClassifier;

    public Example() throws IOException {
        final MultiLayerNetwork simple = NetworkFactory.fromFile(MODEL_PATH + "simple.zip");
        final MultiLayerNetwork convolutional = NetworkFactory.fromFile(MODEL_PATH + "bestModel.bin");
        simpleClassifier = new Classifier(simple);
        convolutionalClassifier = new Classifier(convolutional);
    }

    @Override
    public void start(Stage primaryStage) {

        Canvas canvas = new Canvas(400, 400);
        final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        initDraw(graphicsContext);

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED,
                event -> {
                    graphicsContext.beginPath();
                    graphicsContext.moveTo(event.getX(), event.getY());
                    graphicsContext.stroke();
                });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                event -> {
                    graphicsContext.lineTo(event.getX(), event.getY());
                    graphicsContext.stroke();
                });

        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED,
                event -> {
                    WritableImage writableImage = new WritableImage(400, 400);
                    canvas.snapshot(null, writableImage);
                    final Image scaled = scale(writableImage, 28, 28, false);
                    final ai.nelson.Image image = ai.nelson.Image.fromJavaFx(scaled);
                    System.out.println("Simple..........: " + simpleClassifier.predict(image));
                    System.out.println("Convolutional...: " + convolutionalClassifier.predict(image));
                    System.out.println();
                });

        canvas.addEventHandler(MouseEvent.MOUSE_EXITED,
                event -> {
                    graphicsContext.clearRect(0, 0, 400, 400);
                });

        StackPane root = new StackPane();
        root.getChildren().add(canvas);
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setTitle("No title");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void initDraw(GraphicsContext gc) {
        gc.setFill(Color.RED);
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(48);
        gc.setLineCap(StrokeLineCap.ROUND);
    }

    public javafx.scene.image.Image scale(javafx.scene.image.Image source, int targetWidth, int targetHeight, boolean preserveRatio) {
        ImageView imageView = new ImageView(source);
        imageView.setPreserveRatio(preserveRatio);
        imageView.setFitWidth(targetWidth);
        imageView.setFitHeight(targetHeight);
        return imageView.snapshot(null, null);
    }

}

